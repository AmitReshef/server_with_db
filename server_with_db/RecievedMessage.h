#pragma once

#include "User.h"

using namespace std;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET sock, int messageCode);
	RecievedMessage(SOCKET sock, int messageCode, std::vector<std::string> values);

	//getters
	SOCKET getSock();
	User* getUser();
	int getMessageCode();
	std::vector<std::string>& getValues();

	//setters
	void setUser(User* user);

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	std::vector<std::string> _values;
};